package core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class Driver {

	public static WebDriver driver;

	public static WebDriver instanceDriver() {
		WebDriverManager.chromedriver().setup();
		String[] args = new String[] { "--ignore-ssl-errors=true", "--no-sandbox=true", "--disable-gpu=true" };
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments(args);
		driver = new ChromeDriver(chromeOptions);
		return driver;
	}

	public static void setDriver(){
		driver = instanceDriver();
		driver.get("http://www.google.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
	}

	public static void closeBrowser() {
		driver.quit();
		driver.close();
	}
}
