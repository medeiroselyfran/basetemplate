package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by ruimonteiro on 16-05-2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "features", glue = {
        "stepDefinitions"}, plugin = {"pretty",
        "html:target/cucumber"}, monochrome = true)

public class CukesRunnerTest {
}