package functionalities;

import core.Driver;
import pageObjects.HomePage;

public class HomePageFunc extends Driver {

    private HomePage homePage;

    public HomePageFunc(){
        this.homePage = new HomePage(Driver.driver);
    }

    public void insertTickerValue(String value) {
        this.homePage.getSearchInputField().sendKeys(value);
    }
}
