package stepDefinitions;

import core.Driver;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class CucumberInitialize extends Driver {

    @Before
    public void before() {
        setDriver();
        System.out.println("test");
    }

    @After
    public void after() {
        Driver.closeBrowser();
    }
}
