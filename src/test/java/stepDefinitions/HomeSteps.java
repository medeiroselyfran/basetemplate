package stepDefinitions;

import cucumber.api.java.en.And;
import functionalities.HomePageFunc;

public class HomeSteps {

   private HomePageFunc homePageFunc;

   public HomeSteps() {
      this.homePageFunc = new HomePageFunc();
   }

   @And("^I insert the search text '(.*?)'$")
   public void fillTickerField(String value){
      homePageFunc.insertTickerValue(value);
   }

   @And("^I click on search button$")
   public void clickOnSearchButton(String value){
      homePageFunc.insertTickerValue(value);
   }
}
